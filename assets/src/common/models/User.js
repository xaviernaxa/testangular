/**
 * Created by mmarino on 9/5/2014.
 */
angular.module('models.user', ['lodash', 'services'])
.service('UserModel',['$q', 'lodash', 'utils', '$http', function($q, lodash, utils, $http) {
    this.getAll = function() {
        var deferred = $q.defer();
        var url = utils.prepareUrl('user');

        $http.get(url).then(function(models) {
            return deferred.resolve(models);
        });

        return deferred.promise;
    };

    this.getOne = function(id) {
        var deferred = $q.defer();
        var url = utils.prepareUrl('user/' + id);

        $http.get(url).then(function(model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    };

    this.create = function(newModel) {
        var deferred = $q.defer();
        var url = utils.prepareUrl('user');

        $http.post(url, newModel).then(function(model) {
            return deferred.resolve(model);
        });

        return deferred.promise;
    };
}]);