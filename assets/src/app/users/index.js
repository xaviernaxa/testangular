/**
 * Created by mmarino on 9/5/2014.
 */
angular.module( 'sailng.users', [
    'ngTable'
])
.config( ['$stateProvider', 'USER_ROLES', function config( $stateProvider, USER_ROLES ) {
    $stateProvider.state( 'users', {
        parent: 'base',
        url: '/users',
        views: {
            "main": {
                controller: 'UserCtrl',
                templateUrl: 'users/index.tpl.html'
            }
        },
        data: {
            roles: [USER_ROLES.admin]
        }
    });
}])
.controller( 'UserCtrl',['$scope', 'lodash', 'config', 'titleService', 'UserModel','$filter', 'ngTableParams', function UserController( $scope, lodash, config, titleService, UserModel,$filter, ngTableParams ) {
    titleService.setTitle('User')

    $scope.newUser = {};

    $scope.users = [];
    $scope.currentUser = config.currentUser;

    $scope.destroyUser = function(user) {
        UserModel.delete(user).then(function(model) {
            // todo has been deleted, and removed from $scope.todos
            //   lodash.remove($scope.todos, {id: todo.id});
        });
    };

    UserModel.getAll().then(function(models) {
        $scope.users = models.data;
        var data =$scope.users;
        console.log('data ',data)
        $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10,          // count per page
            sorting: {

                title: 'asc'
            }
        }, {
            total: data.length,
            getData: function($defer, params) {
                var orderedData = params.sorting() ?
                    $filter('orderBy')(data, $scope.tableParams.orderBy()) :
                    data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
    });

}]);
