/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res) {
		User.find().exec(function(err, users) {
			if(err) return res.serverError(err);

			return res.jsonx(users)
		})
	},
	findOne: function(req, res) {
		User.findOne(req.params.id).exec(function(err, user) {
			if(err) return res.serverError(err);
			if(!user) return res.notFound();

			return res.jsonx(user)
		})
	},
	create: function(req, res) {
		User.create(req.params.all(), function(err, user) {
			if(err) return res.serverError(err);
			return res.jsonx(user)
		})
	},
	update: function(req, res) {
		User.update(req.params.id, req.params.all(), function(err, user) {
			if(err) return res.serverError(err);
			return res.jsonx(user)
		})
	},
	destroy: function(req, res) {
		User.destroy(req.params.id, function(err) {
			if(err) return res.serverError(err);
			return res.ok()
		})
	}
};

