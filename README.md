# Sails Vanilla

A [Sails](http://sailsjs.org) application with fontend boilerplate codes using [Sass](http://sass-lang.com), [Angular](http://angularjs.org), [Gulp](http://gulpjs.com), and [Bower](http://bower.io/).

Package used:

- Angular
- Angular UI Router
- Angular UI Moment
- Angular UI Utils
- Angular Bootstrap
- Bootstrap (CSS only)
- jQuery
- Lodash
- Font Awesome
- Sass

# Getting Started

## Installation

1. Clone this repository. `git clone https://bitbucket.org/miehilmie/sails-vanilla.git`
2. Install all dependencies. `npm install`
3. Run the application `sails lift`
4. Done!

## Production
There are few additional tasks added for production including assets concatination, and uglify. To run production mode, use `sails lift --prod` and you will see in the page, there will be only one `production.min.css` and one `production.min.js`

## Change to LESS ?
TODO.

## Additional Configuration
TODO.

> This repo is used by our company, [Basic Inception](http://basicinception.com) to help kick start new node.js project as quickly as possible.


