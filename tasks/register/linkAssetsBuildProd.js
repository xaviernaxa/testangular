module.exports = function (gulp, plugins) {
  gulp.task('linkAssetsBuildProd', function(cb) {
    plugins.sequence(
      'sails-linker-gulp:prodAssets',
      'sails-linker-gulp:prodViews',
      'sails-linker-gulp:prodTplNg',
      cb
    );
  });
};
