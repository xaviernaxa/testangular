module.exports = function (gulp, plugins) {
  gulp.task('syncAssets', function(cb) {
    plugins.sequence(
      'clean:dev',
      'copy:devng',
      'html2js:dev',
      'sass:dev',
      'copy:dev',
      'coffee:dev',
      ['copy:image', 'linkAssets'],
      cb
    );
  });
};
