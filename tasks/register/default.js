module.exports = function (gulp, plugins) {
  gulp.task('default', function(cb) {
    plugins.sequence(
      'compileAssets',
      ['copy:image', 'linkAssets'],
      'watch:dev',
      cb
    );
  });
};
