/**
 * Copy files and folders.
 *
 * ---------------------------------------------------------------
 *
 * # dev task config
 * Copies all directories and files, exept coffescript and less fiels, from the sails
 * assets folder into the .tmp/public directory.
 *
 * # build task config
 * Copies all directories nd files from the .tmp/public directory into a www directory.
 *
 */
module.exports = function(gulp, plugins, growl) {

    // copy all except for sass, image and js
  gulp.task('copy:dev', function() {
    return gulp.src(['./assets/**/*.!(coffee|less|scss)', '!assets/images{,/**}', '!assets/src/{,/**}'])
        .pipe(gulp.dest('.tmp/public'))
        .pipe(plugins.if(growl, plugins.notify({ message: 'Copy dev task complete' })));
  });

    // copy js
    gulp.task('copy:devng', function() {
        return gulp.src(['assets/src/**/*.js'])
            .pipe(gulp.dest('.tmp/public/src'))
            .pipe(plugins.if(growl, plugins.notify({ message: 'Copy dev task complete' })));
    });

    // copy image
  gulp.task('copy:image', function() {
    return gulp.src('assets/images/**/*')
        .pipe(gulp.dest('.tmp/public/images'))
        .pipe(plugins.if(growl, plugins.notify({ message: 'Copy images task complete' })));
  });
  // note: sass file is being compiled to css from sass task

  gulp.task('copy:build', function() {
    return gulp.src('.tmp/public/**/*')
        .pipe(gulp.dest('www'))
        .pipe(plugins.if(growl, plugins.notify({ message: 'Copy build task complete' })));
  });
};
