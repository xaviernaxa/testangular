/**
 * Run predefined tasks whenever watched file patterns are added, changed or deleted.
 *
 * ---------------------------------------------------------------
 *
 * Watch for changes on
 * - files in the `assets` folder
 * - the `tasks/pipeline.js` file
 * and re-run the appropriate tasks.
 *
 *
 */
module.exports = function(gulp, plugins, growl) {

  gulp.task('watch:dev', function(cb) {
    plugins.livereload.listen();

    // Watch assets
    gulp.watch(['api/**/*', 'tasks/pipeline.js', 'assets/**/*.!(coffee|less|scss)', '!assets/images{,/**}', '!assets/src/{,/**}'])
      .on('change', function(file) {
        plugins.sequence(
          'syncAssets',
          function() {
            plugins.livereload.changed(file.path);
          }
        );
      });

    // sass
    gulp.watch('assets/styles/**/*.scss')
      .on('change', function(file) {
        plugins.sequence(
          'sass:dev',
          'linkAssets',
          function() {
            plugins.livereload.changed(file.path);
          }
        );
      });

    // image
    gulp.watch('assets/images/**/*')
      .on('change', function(file) {
        plugins.sequence(
          'copy:image',
          function() {
            plugins.livereload.changed(file.path);
          }
        );
      });


    // image
    gulp.watch('assets/src/**/*')
      .on('change', function(file) {
        plugins.sequence(
          'copy:devng',
          'html2js:dev',
          'linkAssets',
          function() {
            plugins.livereload.changed(file.path);
          }
        );
      });

  });

};
